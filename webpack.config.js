var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: ['./app/index.js', './sass/test.scss'],
  output: {
    filename: 'public/[name].js'
  },
  module: {
      loaders: [
          {
              test: /\.js$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
              query: {
                  presets: ['es2015']
              }
          },
          {
              test: /\.css$/,
              exclude: /node_modules/,
              loader: 'style-loader!css-loader'
          },
          {
              test: /\.scss$/,
              loader: ExtractTextPlugin.extract('css-loader!sass-loader')
          }
      ]
  },
  plugins: [
      new ExtractTextPlugin({ filename: 'public/[name].css', allChunks: true }),
  ],
};